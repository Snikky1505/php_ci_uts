<?php
/**
 * Created by PhpStorm.
 * User: Nikko
 * Date: 03/04/2018
 * Time: 19:08
 */
//echo "<pre>";
//print_r($all->judul);
//echo "</pre>";
//die();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Program Sistem Perpustakaan</title>

	<meta name="description" content="This is page-header (.page-header &gt; h1)" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!--basic styles-->

	<link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/css/bootstrap-responsive.min.css')?>" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>" />
	<!--page specific plugin styles-->

	<link rel="stylesheet" href="assets/css/prettify.css" />

	<!--fonts-->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
	<!--ace styles-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/ace.min.css')?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/ace-responsive.min.css')?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/ace-skins.min.css')?>" />

	<!--inline styles related to this page-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<div class="navbar">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a href="#" class="brand">
				<small>
					<i class="icon-book"></i>
					Sistem Perpustakaan
				</small>
			</a><!--/.brand-->

			<ul class="nav ace-nav pull-right">
				<li class="light-blue">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<img class="nav-user-photo" src="<?php echo base_url('assets/avatars/user.jpg')?>" alt="Jason's Photo" />
						<span class="user-info">
									<small>Welcome,</small>
									<?php echo $this->session->kduser;?>
								</span>

						<i class="icon-caret-down"></i>
					</a>

					<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
						<li>
							<a href="<?php echo base_url('Login_Ctrl/shutsession')?>">
								<i class="icon-off"></i>
								Logout
							</a>
						</li>
					</ul>
				</li>
			</ul><!--/.ace-nav-->
		</div><!--/.container-fluid-->
	</div><!--/.navbar-inner-->
</div>

<div class="main-container container-fluid">
	<a class="menu-toggler" id="menu-toggler" href="#">
		<span class="menu-text"></span>
	</a>

	<div class="sidebar" id="sidebar">
		<ul class="nav nav-list">
			<li class="active">
				<a href="<?php echo base_url('HomeCtrl')?>">
					<i class="icon-dashboard"></i>
					<span class="menu-text"> Dashboard </span>
				</a>
			</li>
			<?php if($this->session->status=="1" || $this->session->status == "2") { ?>
				<li>
					<a href="<?php echo base_url('BukuCtrl')?>">
						<i class="icon-desktop"></i>
						<span class="menu-text"> Tambah Buku </span>
					</a>
				</li>
			<?php }else { ?>

			<?php } ?>

		</ul><!--/.nav-list-->

		<div class="sidebar-collapse" id="sidebar-collapse">
			<i class="icon-double-angle-left"></i>
		</div>
	</div>

	<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="icon-home home-icon"></i>
					<a href="#">Home</a>

					<span class="divider">
						<i class="icon-angle-right arrow-icon"></i>
					</span>
				</li>
				<li>
					Dashboard
					<span class="divider">
						<i class="icon-angle-right arrow-icon"></i>
					</span>
				</li>
				<li class="active">View</li>
			</ul><!--.breadcrumb-->


		</div>

		<div class="page-content">
			<div class="row-fluid">
				<div class="span12">
					<!--PAGE CONTENT BEGINS-->

					<div class="row-fluid">
						<div class="span12">
							<div class="widget-box">
								<div class="widget-header widget-header-flat">
									<h4>Detail Buku</h4>
								</div>

								<div class="widget-body">
									<div class="widget-main">
										<div class="container-fluid">
<!--											<div class="span6">-->
												<table style="font-size: 14pt;">
													<tr>
														<td>Kode Buku</td>
														<td>:</td>
														<td><?php echo $all->kdbuku?></td>
													</tr>
													<tr>
														<td>Judul Buku</td>
														<td> : </td>
														<td><?php echo $all->judul?></td>
													</tr>
													<tr>
														<td>Pengarang</td>
														<td>:</td>
														<td><?php echo $all->pengarang?></td>
													</tr>
													<tr>
														<td>Tanggal Terbit</td>
														<td>:</td>
														<td><?php echo $all->tglterbit?></td>
													</tr>
													<tr>
														<td>Penerbit</td>
														<td>:</td>
														<td><?php echo $all->penerbit?></td>
													</tr>
													<tr>
														<td>Sinopsis</td>
														<td>:</td>
														<td><?php echo $all->sinopsis;?></td>
													</tr>
												</table>
												<a href="<?php echo base_url('HomeCtrl')?>" class="btn btn-primary"><i class="icon-angle-left arrow-icon"></i>Go Back</a>
<!--											</div>-->
										</div>
									</div>
								</div>
							</div>
						</div><!--/span-->
					</div>
						<!--PAGE CONTENT ENDS-->

				</div><!--/.span-->
			</div><!--/.row-fluid-->
		</div><!--/.page-content-->
	</div><!--/.main-content-->
</div><!--/.main-container-->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
	<i class="icon-double-angle-up icon-only bigger-110"></i>
</a>

<!--basic scripts-->

<!--[if !IE]>-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

<!--<![endif]-->

<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]>-->

<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>

<!--<![endif]-->

<!--[if IE]>
<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
	if("ontouchend" in document) document.write("<script src='<?php echo base_url('assets/js/jquery.mobile.custom.min.js')?>'>"+"<"+"/script>");
</script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>

<!--page specific plugin scripts-->

<script src="<?php echo base_url('assets/js/prettify.js')?>"></script>

<!--ace scripts-->

<script src="<?php echo base_url('assets/js/ace-elements.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/ace.min.js')?>"></script>

<!--inline scripts related to this page-->

<script type="text/javascript">
	$(function() {

		window.prettyPrint && prettyPrint();
		$('#id-check-horizontal').removeAttr('checked').on('click', function(){
			$('#dt-list-1').toggleClass('dl-horizontal').prev().html(this.checked ? '&lt;dl class="dl-horizontal"&gt;' : '&lt;dl&gt;');
		});

	})
</script>
</body>
</html>

