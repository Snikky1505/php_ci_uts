<?php
/**
 * Created by PhpStorm.
 * User: Nikko
 * Date: 02/04/2018
 * Time: 13:41
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Program Sistem Perpustakaan</title>

	<meta name="description" content="User login page" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!--basic styles-->

	<link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/css/bootstrap-responsive.min.css')?>" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>" />

	<!--page specific plugin styles-->

	<!--fonts-->

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

	<!--ace styles-->

	<link rel="stylesheet" href="<?php echo base_url('assets/css/ace.min.css')?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/ace-responsive.min.css')?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/ace-skins.min.css')?>" />
	<!--inline styles related to this page-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="login-layout">
<div class="main-container container-fluid">
	<div class="main-content">
		<div class="row-fluid">
			<div class="span12">
				<div class="login-container">
					<div class="row-fluid">
						<div class="center">
							<h1>
								<i class="icon-book blue"></i>
								<span class="red">Snikky</span>
							</h1>
							<h4 class="blue">&copy; Snikky Corp</h4>
						</div>
					</div>

					<div class="space-6"></div>

					<div class="row-fluid">
						<div class="position-relative">
							<div id="login-box" class="login-box visible widget-box no-border">
								<center class="widget-body">
									<div class="widget-main">
										<h4 class="header blue lighter bigger">
											<i class="icon-coffee green"></i>
											Please Login
										</h4>

										<div class="space-6"></div>

										<?php echo (!isset($error)) ? "" : "<span style='color:red;font-size:10pt !important'>".$error."</span>"?>
										<?php echo form_open('Dashboard')?>
										<form >
										<fieldset>
											<label>
												<span class="block input-icon input-icon-right">
													<input type="text" id="kduser" name="kduser" class="span12" placeholder="Username" required/>
													<i class="icon-user"></i>
												</span>
											</label>

											<label>
												<span class="block input-icon input-icon-right">
													<input type="password" id="password" name="password" class="span12" placeholder="Password" required />
													<i class="icon-lock"></i>
												</span>
											</label>

											<div class="space"></div>

											<center>
												<div class="clearfix">
													<button type="submit" class="width-35 btn btn-small btn-primary">
														<i class="icon-key"></i>
														Login
													</button>
												</div>
											</center>
											<div class="space-4"></div>
										</fieldset>
										</form>
										<?php echo form_close()?>

									</div><!--/widget-main-->
										<div class="toolbar clearfix" style="position: center; color: gold; padding: 10px">
<!--											<div class="forgot-password-link" style="position: center">-->
												Copyright &copy; Nikko Handiarto

										</div>
									</div>
								</div><!--/widget-body-->
							</div><!--/login-box-->

						</div><!--/position-relative-->
					</div>
				</div>
			</div><!--/.span-->
		</div><!--/.row-fluid-->
	</div>
</div><!--/.main-container-->

<!--basic scripts-->

<!--[if !IE]>-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

<!--<![endif]-->

<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]>-->

<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>

<!--<![endif]-->

<!--[if IE]>
<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
	if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="assets/js/bootstrap.min.js"></script>

<!--page specific plugin scripts-->

<!--ace scripts-->

<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<!--inline scripts related to this page-->

<script type="text/javascript">
	function show_box(id) {
		$('.widget-box.visible').removeClass('visible');
		$('#'+id).addClass('visible');
	}
</script>
</body>
</html>

