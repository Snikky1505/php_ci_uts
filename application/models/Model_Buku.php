<?php
/**
 * Created by PhpStorm.
 * User: Nikko
 * Date: 03/04/2018
 * Time: 9:29
 */
class Model_Buku extends CI_Model
{
	public function __construct()
	{
		parent:: __construct();
	}
	public function cekSession(){
		if($this->session->userdata('status') == null){
			return false;
		}else{
			return true;
		}
	}
	public function getAllBuku()
	{
		$hasil = $this->db->get('buku');
		return $hasil->result();
	}
	public function get1Buku($id)
	{
		$hasil = $this->db->where('kdbuku',$id)->get('buku')->row();
		return $hasil;
	}
	public function insert($data)
	{
		$insertcheck = false;
		try{
			$this->db->insert('buku', $data);
			$insertcheck = true;
		}catch(Exception $ex){
			$insertcheck = false;
		}
		return $insertcheck;
	}
	public function deleteBuku($data)
	{
		try{
			$this->db->where('kdbuku', $data);
			$q = $this->db->delete('buku');
		}catch(Exception $die){
			die($die);
		}
		return $q;
	}
	public function update($kode,$data)
	{
		try{
			$this->db->where('kdbuku', $kode);
			$q = $this->db->update('buku',$data);
		}catch(Exception $e){
			die($e);
		}
		return $q;
	}
}
