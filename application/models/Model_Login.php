<?php
/**
 * Created by PhpStorm.
 * User: Nikko
 * Date: 02/04/2018
 * Time: 13:34
 */
class Model_Login extends CI_Model
{
	public function readUser($kduser, $password){
		$hasil = $this->db->where('kduser', $kduser)->where('password', $password)->limit(1)->get('user');
		return $hasil->row();
	}
	public function getUser(){
		$hasil = $this->db->get('user');
		return $hasil->result();
	}
	public function cekSession(){
		if($this->session->userdata('status') == null){
			return false;
		}else{
			return true;
		}
	}
	public function checklogin($data){
		return $this->db->where('kduser',$data['kduser'])->where('password',$data['password'])->get('user')->row();
	}


}
