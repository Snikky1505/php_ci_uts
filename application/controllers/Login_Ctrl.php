<?php
/**
 * Created by PhpStorm.
 * User: Nikko
 * Date: 02/04/2018
 * Time: 13:31
 */
class Login_Ctrl extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('Model_Login');
	}

	public function index()
	{
		$this->load->view('LoginForm');
	}
	public function authent()
	{
		try{
			$data = array(
				'kduser' => $this->input->post('kduser'),
				'password' => $this->input->post('password')
			);
			$check  = $this->Model_Login->checklogin($data);
			if($check != null){
				if($this->makeSession($check) == true){
					$this->session->userdata('status');
					redirect('HomeCtrl', 'refresh');
				}
			}else{
				$data['error'] = "Username dan password yang dimasukkan Salah!";
				$this->load->view('LoginForm', $data);

			}
		}catch (Exception $e){
			die($e);
		}
	}
	public function makeSession($db){
		try{
			$sessiondata = array(
				'status' => $db->status,
				'kduser' => $db->kduser,
//				md5('email') => $db->email,
			);
		}catch (Exception $e){
			die($e);
		}
		$this->session->set_userdata($sessiondata);
		return true;
	}
	public function shutsession()
	{
		$this->session->sess_destroy();
		redirect('Login_Ctrl', 'refresh');
	}
	public function getArtikel()
	{
		if($this->session->status==NULL){
			redirect('');
		}
		$allBuku = $this->Model_Login->getAllBuku();
		$kduser = $this->session->kduser;
		$authent = $this->session->status;
		$data['kduser'] = $kduser;
		$data['databuku'] = $allBuku;
//		$data['menu'] = $menu;
		$data['status'] = $authent;
		$this->load->view('HomeForm',$data);
	}
}
