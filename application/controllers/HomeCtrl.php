<?php
/**
 * Created by PhpStorm.
 * User: Nikko
 * Date: 03/04/2018
 * Time: 13:22
 */
class HomeCtrl extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('Model_Buku');
	}
	public function index()
	{
		if($this->Model_Buku->cekSession() == true)
		{
			$data['all'] = $this->Model_Buku->getAllBuku();
			$this->load->view('HomeForm', $data);
		}
	}
}
