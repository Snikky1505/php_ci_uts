<?php
/**
 * Created by PhpStorm.
 * User: Nikko
 * Date: 02/04/2018
 * Time: 19:24
 */
class BukuCtrl extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('Model_Login');
		$this->load->model('Model_Buku');
	}
	public function index()
	{
		$data['kdbuku'] = $this->autonumber();
		$this->load->view('TambahBuku',$data);
	}
	public function Dashboard()
	{
		$this->load->view('HomeForm');
	}
	public function autonumber()
	{
		$hasil = '';
			$temp_max = 0;
				$db = $this->db->select('ifnull(max(convert(right(kdbuku, 7),signed integer)),0) as kode')->get('buku')->row();
				$temp_max = ($db->kode)+1;
				if($db->kode != 0)
				{
					$hasil = "000000" . $temp_max;
					$hasil = "BKU" . substr($hasil,(strlen($hasil))-7);
				}else{
					$hasil = "BKU0000001";
				}
				return $hasil;
	}
	public function tambah(){
		$data = array(
			'kdbuku' => $this->autonumber(),
			'judul' => $this->input->post('judul'),
			'pengarang' =>$this->input->post('pengarang'),
			'sinopsis' => $this->input->post('sinopsis'),
			'tglterbit' => $this->input->post('tglterbit'),
			'penerbit' => $this->input->post('penerbit')
		);
		$hasil = $this->Model_Buku->insert($data);
		$data = null;
		if($hasil){
			$data['hasil'] = "sukses";
		}else{
			$data['hasil'] = "gagal";
		}
		redirect('HomeCtrl', 'refresh');
	}
	public function update()
	{
		if($this->Model_Buku->cekSession() == true){
			$id = $this->uri->segment(2);
			$data['all'] = $this->Model_Buku->get1Buku($id);

			$this->load->view('EditBuku', $data);
		}

	}
	public function update1(){
		if($this->Model_Buku->cekSession() == true){
			$kdbuku = $this->uri->segment(2);
			$data = array(
				'judul' => $this->input->post('judul'),
				'pengarang' =>$this->input->post('pengarang'),
				'sinopsis' => $this->input->post('sinopsis'),
				'tglterbit' => $this->input->post('tglterbit'),
				'penerbit' => $this->input->post('penerbit')
			);
			$query = $this->Model_Buku->update($kdbuku,$data);
			if($query==true){
//				print_r($data);
//				die();
				redirect('HomeCtrl','refresh');
			}

		}
	}
	public function delete()
	{
		if($this->Model_Buku->cekSession() == true){
			$kode = $this->uri->segment(2);
			$query = $this->Model_Buku->deleteBuku($kode);
			if($query==true){
				redirect('HomeCtrl','refresh');
			}
		}
	}
	public function view()
	{
		if($this->Model_Buku->cekSession() == true){
			$id = $this->uri->segment(2);
			$data['all'] = $this->Model_Buku->get1Buku($id);

			$this->load->view('TampilanBuku', $data);
		}
	}
	public function temp()
	{
		$this->load->view('EditBuku');
	}
}
